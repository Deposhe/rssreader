//
//  RSTestCoreData.h
//  RSNewsReader
//
//  Created by Mobexs on 7/18/16.
//  Copyright © 2016 Serge Rylko. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RSTestCoreData : NSObject
@property (readonly, strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (readonly, strong, nonatomic) NSManagedObjectModel *managedObjectModel;
@property (readonly, strong, nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinator;
+ (RSTestCoreData *)sharedTestManager;
- (void)saveContext;
- (NSURL *)applicationDocumentsDirectory;
@end
