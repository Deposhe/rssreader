//
//  MasterViewController+testController.m
//  RSNewsReader
//
//  Created by Mobexs on 7/13/16.
//  Copyright © 2016 Serge Rylko. All rights reserved.
//

#import "MasterViewController+testController.h"
#import "MNews.h"
#import "RSNewsManager.h"
#import "RSTestCoreData.h"
#import "RSNewsManager.h"
#import "RSStoreManager.h"


@implementation MasterViewController (testController)
/*
- (void) addTestFeatures
{
    UIBarButtonItem * addButton = [[UIBarButtonItem alloc] initWithTitle:@"ADD NEWS"
                                                                   style:UIBarButtonItemStylePlain
                                                                  target:self
                                                                  action:@selector(generateTestNews)];
   
    
    UIBarButtonItem * changeNewsButton = [[UIBarButtonItem alloc] initWithTitle:@"CHANGE"
                                                                   style:UIBarButtonItemStylePlain
                                                                  target:self
                                                                  action:@selector(changeNews:)];
    
    self.navigationItem.rightBarButtonItems = @[addButton, changeNewsButton];    
}
 */



#pragma mark - Test to delete
/*
- (MNews*) generateTestNews
{
    MNews * news = [MNews MR_createEntity];
    
    news.mobDescription = @"Some detail text\n Some detail text\n Some detail text\n Some detail text\n";
    news.mobName = [NSString stringWithFormat:@"Latest News - %u", arc4random() % 500];
    news.mobNewsDate = [[NSDate alloc] initWithTimeIntervalSinceNow:0.0];
    
    
    [self saveContext];
    return news;
}


- (void)saveContext {
    NSError * error = nil;
    [[[RSNewsManager sharedManager] defaultManagementObjectContext] saveToPersistentStore:&error];
    
    if (error) {
        NSLog(@"%@", [error  localizedDescription]);
    }
    else
    {
        NSLog(@"Saved by persistent store");
    }
}


- (void) changeNews: (NSInteger) indexOfNews
{
    NSArray * victimsArray = [MNews MR_findAllSortedBy:@"mobNewsDate" ascending:NO];
    MNews * newsToSave = [victimsArray objectAtIndex:20];
    NSLog(@"CHANGED - name - %@", newsToSave.mobName);
    [MagicalRecord saveWithBlock:^(NSManagedObjectContext *localContext) {
        MNews * news = [newsToSave MR_inContext:localContext];
        news.mobDescription = @"-->>\n -->>\n -->>\n";
        
    }];
    
    NSLog(@"CHANGED - name - %@", newsToSave.mobName);
}
 
 */



/////------------------- TESTED ----- >
- (void) addTestButton
{
    UIBarButtonItem * item = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAdd target:self action:@selector(addNewsToData)];
    self.navigationItem.leftBarButtonItem = item;
}
- (void) addTestedItemsToData
{
    //[self.newsManager testMultipleAdding];
}


- (void) changeNewsName
{
    NSLog(@"Change");
    NSManagedObjectContext * context = [[RSStoreManager sharedStoreManager] managedObjectContext];
    NSFetchRequest * request = [[NSFetchRequest alloc] initWithEntityName:NSStringFromClass([MNews class])];
    NSSortDescriptor * dateDescriptor = [NSSortDescriptor sortDescriptorWithKey:@"mobNewsDate" ascending:NO];
    NSPredicate * predicate = [NSPredicate predicateWithFormat:@"mobDetailURL contains[c] %@", @"tut.by"];
    [request setPredicate:predicate];
    [request setSortDescriptors:@[dateDescriptor]];
    NSError * error = nil;
    MNews * news = [[context executeFetchRequest:request error:&error] firstObject];
    //MNews * news = [[array firstObject] firstObject];
    
    NSLog(@"NAME - %@", news.mobName);
    NSError * saveError = nil;

    news.mobDescription = @"changed";
    [context save:&saveError];    
};

- (void) addNewsToData
{
    [self.newsManager addSomeItems];
}

//////---------------- TESTED END----- <


@end
