//
//  BaseViewController.m
//  RSNewsReader
//
//  Created by Mobexs on 7/11/16.
//  Copyright © 2016 Serge Rylko. All rights reserved.
//

#import "BaseViewController.h"

@interface BaseViewController ()

@property (nonatomic, weak) UIActivityIndicatorView * activityIndicator;

@end



@implementation BaseViewController


#pragma mark - Activity indicator

- (void)animateActivityIndicator:(BOOL)animate
{
    if (!self.activityIndicator)
    {
        UIActivityIndicatorView * activityIndicator = [[UIActivityIndicatorView alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth(self.view.frame), CGRectGetHeight(self.view.frame))];
        activityIndicator.backgroundColor = [UIColor colorWithWhite:0 alpha:0.1];
        activityIndicator.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
        activityIndicator.activityIndicatorViewStyle = UIActivityIndicatorViewStyleWhiteLarge;
        self.activityIndicator = activityIndicator;
        [self.view addSubview:activityIndicator];
    }
    self.activityIndicator.hidden = !animate;
    
    if (animate)
    {
        [self.view bringSubviewToFront:self.activityIndicator];
        [self.activityIndicator startAnimating];
    }
    else
        [self.activityIndicator stopAnimating];
}

@end
