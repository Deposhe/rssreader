//
//  RSOnlinerManager.m
//  RSNewsReader
//
//  Created by Mobexs on 7/15/16.
//  Copyright © 2016 Serge Rylko. All rights reserved.
//

#import "RSOnlinerManager.h"




@implementation RSOnlinerManager

@synthesize path = _path;
@synthesize objectManager = _objectManager;


#pragma mark - Manager Settings Configuring


- (void) configureDataManager
{
    NSLog(@"CONFIGURING ONLINER DATAMANAGER");
    NSURL *baseURL = [NSURL URLWithString:@"https://tech.onliner.by/feed/"];
    
    NSString * mimeTypeString = @"text/xml";
    
    RKObjectManager * objectManager = [RKObjectManager managerWithBaseURL:baseURL];
    [RKMIMETypeSerialization registerClass:[RKXMLReaderSerialization class] forMIMEType:mimeTypeString];

    [[AFNetworkActivityIndicatorManager sharedManager] setEnabled:YES];
   
    _objectManager = objectManager;
    _path = @"";
}



#pragma mark - Getting data

- (void) detailNewsTextFromURLString: (NSString *) urlString
                             success: (void (^) (NSString * text)) success
                             failure: (void (^) (NSError * error)) failure
{
    NSLog(@"GET Detail ONLINER");
    NSURL *URL = [NSURL URLWithString:urlString];
    NSURLSession *session = [NSURLSession sharedSession];
    
    [[session dataTaskWithURL:URL completionHandler:
      ^(NSData *data, NSURLResponse *response, NSError *error) {
          
          //NSLog(@"RESPONSE - %@", response);
          NSString *contentType = nil;
          if ([response isKindOfClass:[NSHTTPURLResponse class]]) {
              NSDictionary *headers = [(NSHTTPURLResponse *)response allHeaderFields];
              contentType = headers[@"Content-Type"];
          }
          HTMLDocument *home = [HTMLDocument documentWithData:data
                                            contentTypeHeader:contentType];
          
          NSString * cssSelector = @"div.b-posts-1-item__text > p";
          HTMLArrayOf(HTMLElement *) *div = [home nodesMatchingSelector:cssSelector];
          NSMutableString * preString = [NSMutableString string];
          
          for (HTMLElement* element in div) {
              preString = [[preString stringByAppendingString:element.textContent] mutableCopy];
              preString = [[preString stringByAppendingString:@"\n\n"] mutableCopy];
          }
          
          NSCharacterSet *whitespace = [NSCharacterSet whitespaceAndNewlineCharacterSet];
          NSString * trimmedString = [preString stringByTrimmingCharactersInSet:whitespace] ;
          
          //NSLog(@"HTML PARSING COMPLETION RESULT - %@", trimmedString);
          if (!error) {
              success (trimmedString);
          } else {
              //NSLog(@"HTML PARSE ERROR - %@", error.localizedDescription);
              failure (error);
          }
          
      }] resume];
}



#pragma mark - SetMapping

- (void) setNewsMapping
{
    NSLog(@"ONliner Mapping set");
    NSDictionary * parseValuesDictionary = [MNews getOnlinerNewsDictionnary];
    [self addMappingForEntityForName:NSStringFromClass([MNews class])
 andAttributeMappingsFromDictionnary:parseValuesDictionary
         andIdentificationAttributes:@[@"mobName"]
                      andPathPattern:nil
                          andKeyPath:@"rss.channel.item"];
}




@end
