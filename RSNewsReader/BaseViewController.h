//
//  BaseViewController.h
//  RSNewsReader
//
//  Created by Mobexs on 7/11/16.
//  Copyright © 2016 Serge Rylko. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BaseViewController : UIViewController


@property (nonatomic, weak, readonly) UIActivityIndicatorView * activityIndicator;

- (void)animateActivityIndicator:(BOOL)animate;

@end
