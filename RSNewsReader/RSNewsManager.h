//
//  RSNewsManager.h
//  RSNewsReader
//
//  Created by Сергей on 23.06.16.
//  Copyright © 2016 Serge Rylko. All rights reserved.
//

#import <Foundation/Foundation.h>

#import <RestKit/RestKit.h>
#import <RestKit/CoreData.h>
#import "MNews.h"
#import <XMLReader/XMLReader.h>
#import "RKXMLReaderSerialization.h"
#import "HTMLReader.h"



@protocol RSNewsManagerProtocol;

@protocol RSNewsManagerProtocol <NSObject>

@required

- (void) configureDataManager;
- (void) setNewsMapping;
- (void) detailNewsTextFromURLString: (NSString *) urlString
                             success: (void (^) (NSString * text)) success
                             failure: (void (^) (NSError * error)) failure;

@end



@interface RSNewsManager : NSObject 

@property (nonatomic, strong, readonly) RKObjectManager * objectManager;
@property (nonatomic, strong, readonly) NSString * path;



#pragma mark - General setup

- (void) setUpObjectStore;

- (NSFetchedResultsController*) configureNewsFetchResultController;

- (void) switchFetchPredicate: (NSFetchedResultsController * ) fetchedResultsController;



#pragma mark - General Mapping interface

- (void) addMappingForEntityForName: (NSString *) entityName
andAttributeMappingsFromDictionnary: (NSDictionary *) attributeMappingsDictionary
        andIdentificationAttributes: (NSArray *) idAttributes
                     andPathPattern: (NSString *) pathPattern
                         andKeyPath:(NSString *) keyPathString;



#pragma mark - Data Loading

- (void) loadObjectsSuccess:(void (^) (void)) success failure:(void (^) (void)) failure;



#pragma mark - Virtuals methods

- (void) configureDataManager;

- (void) setNewsMapping;

- (void) detailNewsTextFromURLString: (NSString *) urlString
                             success: (void (^) (NSString * text)) success
                             failure: (void (^) (NSError * error)) failure; // virtual



#pragma mark - Manage News

- (void) removeNews: (MNews*) news;

///------ tested
- (void) addSomeItems;

@end





