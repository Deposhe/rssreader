//
//  MNews+CoreDataProperties.h
//  RSNewsReader
//
//  Created by Сергей on 23.06.16.
//  Copyright © 2016 Serge Rylko. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "MNews.h"

NS_ASSUME_NONNULL_BEGIN

@interface MNews (CoreDataProperties)

@property (nullable, nonatomic, retain) NSString *mobDescription;
@property (nullable, nonatomic, retain) NSString *mobDetailURL;

@property (nullable, nonatomic, retain) NSString *mobIconImageURL;
@property (nullable, nonatomic, retain) NSString *mobName;
@property (nullable, nonatomic, retain) NSDate *mobNewsDate;
@property (nullable, nonatomic, retain) NSString *mobDetailNews;

@end

NS_ASSUME_NONNULL_END
