//
//  NSString+RSNews.m
//  RSNewsReader
//
//  Created by Сергей on 22.06.16.
//  Copyright © 2016 Serge Rylko. All rights reserved.
//

#import "NSString+RSNews.h"


@implementation NSString (RSNews)


#pragma mark - shared objects

+ (NSDateFormatter *) sharedFormatter
{
    static NSDateFormatter * dateFormatter = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"EEE, dd MMM YYYY  hh:mm"];
    });
    
    return dateFormatter;
}



#pragma mark - Modificate strings

+ (NSString *) dateStringFromDate: (NSDate *) date
{
    NSString * dateString = [[NSString sharedFormatter] stringFromDate:date];
    
    return dateString;
}


+ (NSString *)stripTags:(NSString *)str
{
    NSMutableString *html = [NSMutableString stringWithCapacity:[str length]];
    
    NSScanner *scanner = [NSScanner scannerWithString:str];
    scanner.charactersToBeSkipped = NULL;
    NSString *tempText = nil;
    
    while (![scanner isAtEnd])
    {
        [scanner scanUpToString:@"<" intoString:&tempText];
        
        if (tempText != nil)
            [html appendString:tempText];
        
        [scanner scanUpToString:@">" intoString:NULL];
        
        if (![scanner isAtEnd])
            [scanner setScanLocation:[scanner scanLocation] + 1];
        
        tempText = nil;
    }
    
    return html;
}





@end
