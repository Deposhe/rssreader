//
//  RSStoreManager.m
//  RSNewsReader
//
//  Created by Mobexs on 7/20/16.
//  Copyright © 2016 Serge Rylko. All rights reserved.
//

#import "RSStoreManager.h"

@interface RSStoreManager ()

@property (nonatomic, strong) RKManagedObjectStore * store;

@end


@implementation RSStoreManager

+ (id) sharedStoreManager
{
    static RSStoreManager * storeManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        storeManager = [[RSStoreManager alloc] init];
    });
    
    return storeManager;
}

- (instancetype)init
{
    self = [super init];
    if (self)
    {
        RKManagedObjectStore * managedObjectStore = [[RKManagedObjectStore alloc] init];
        NSURL *modelURL = [NSURL fileURLWithPath:[[NSBundle mainBundle] pathForResource:@"RSNewsReader" ofType:@"momd"]];
        NSManagedObjectModel *managedObjectModel = [[[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL] mutableCopy];
        managedObjectStore = [[RKManagedObjectStore alloc] initWithManagedObjectModel:managedObjectModel];
        
        NSString *storePath = [RKApplicationDataDirectory() stringByAppendingPathComponent:@"RSNewsReader.sqlite"];
        NSError *error = nil;
        [managedObjectStore addSQLitePersistentStoreAtPath:storePath fromSeedDatabaseAtPath:nil withConfiguration:nil options:nil error:&error];
        [managedObjectStore createManagedObjectContexts];
        
        _store = managedObjectStore;
        _managedObjectContext = managedObjectStore.mainQueueManagedObjectContext;
        _saveManagedObjectContext = [managedObjectStore newChildManagedObjectContextWithConcurrencyType:NSPrivateQueueConcurrencyType tracksChanges:NO];
    }
    return self;
}








@end
