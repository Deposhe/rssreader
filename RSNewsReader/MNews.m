//
//  MNews.m
//  RSNewsReader
//
//  Created by Сергей on 23.06.16.
//  Copyright © 2016 Serge Rylko. All rights reserved.
//

#import "MNews.h"

@implementation MNews

+ (NSDictionary *) getTutMappingDictionnary
{
    NSDictionary * dict = @{@"title.text" : @"mobName",
                            @"description.text" : @"mobDescription",
                            @"enclosure.url" : @"mobIconImageURL",
                            @"link.text" : @"mobDetailURL",
                            @"pubDate.text" : @"mobNewsDate"};
    return dict;
}


+ (NSDictionary *) getOnlinerNewsDictionnary
{
    NSDictionary * dict = @{@"title.text" : @"mobName",
                            @"description.text" : @"mobDescription",
                            @"media:thumbnail.url" : @"mobIconImageURL",
                            @"link.text" : @"mobDetailURL",
                            @"pubDate.text" : @"mobNewsDate"};
    return dict; 
}

@end
