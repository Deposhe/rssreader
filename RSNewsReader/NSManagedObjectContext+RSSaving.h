//
//  NSManagedObjectContext+RSSaving.h
//  RSNewsReader
//
//  Created by Mobexs on 7/22/16.
//  Copyright © 2016 Serge Rylko. All rights reserved.
//

#import <CoreData/CoreData.h>

@interface NSManagedObjectContext (RSSaving)

- (void) smartSaveBlock: (void (^) (NSManagedObjectContext * localContext)) saveBlock;

@end
