//
//  MasterViewController.h
//  RSNewsReader
//
//  Created by Сергей on 23.06.16.
//  Copyright © 2016 Serge Rylko. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"
#import "RSNewsManager.h"

@interface MasterViewController : BaseViewController

@property (nonatomic, readonly) RSNewsManager * newsManager;

@end
