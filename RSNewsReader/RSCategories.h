//
//  RSCategories.h
//  RSNewsReader
//
//  Created by Сергей on 22.06.16.
//  Copyright © 2016 Serge Rylko. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NSString+RSNews.h"
#import "NSManagedObjectContext+RSSaving.h"

@interface RSCategories : NSObject

@end
