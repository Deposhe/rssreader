//
//  MNews+CoreDataProperties.m
//  RSNewsReader
//
//  Created by Сергей on 23.06.16.
//  Copyright © 2016 Serge Rylko. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "MNews+CoreDataProperties.h"

@implementation MNews (CoreDataProperties)

@dynamic mobDescription;
@dynamic mobDetailURL;
@dynamic mobIconImageURL;
@dynamic mobName;
@dynamic mobNewsDate;
@dynamic mobDetailNews;

@end
