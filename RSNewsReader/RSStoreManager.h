//
//  RSStoreManager.h
//  RSNewsReader
//
//  Created by Mobexs on 7/20/16.
//  Copyright © 2016 Serge Rylko. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "RestKit.h"

@interface RSStoreManager : NSObject


@property (nonatomic, strong, readonly) RKManagedObjectStore * store;
@property (nonatomic, strong, readonly) NSManagedObjectContext * managedObjectContext;
@property (nonatomic, strong, readonly) NSManagedObjectContext * saveManagedObjectContext;


+ (RSStoreManager *) sharedStoreManager;


@end
