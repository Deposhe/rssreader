//
//  MNews.h
//  RSNewsReader
//
//  Created by Сергей on 23.06.16.
//  Copyright © 2016 Serge Rylko. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

NS_ASSUME_NONNULL_BEGIN

@interface MNews : NSManagedObject

+ (NSDictionary *) getTutMappingDictionnary;
+ (NSDictionary *) getOnlinerNewsDictionnary;

@end

NS_ASSUME_NONNULL_END

#import "MNews+CoreDataProperties.h"
