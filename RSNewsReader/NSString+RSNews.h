//
//  NSString+RSNews.h
//  RSNewsReader
//
//  Created by Сергей on 22.06.16.
//  Copyright © 2016 Serge Rylko. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (RSNews)

+ (NSString *) dateStringFromDate: (NSDate *) date;
+ (NSString *)stripTags:(NSString *)str;

@end
