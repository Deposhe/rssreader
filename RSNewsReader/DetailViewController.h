//
//  RSDetailViewControlelr.h
//  RSNewsReader
//
//  Created by Сергей on 23.06.16.
//  Copyright © 2016 Serge Rylko. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MNews.h"
#import "BaseViewController.h"

@interface DetailViewController : BaseViewController

@property (nonatomic, strong) MNews * detailNews;

@end
