//
//  RSNewsManager.m
//  RSNewsReader
//
//  Created by Сергей on 23.06.16.
//  Copyright © 2016 Serge Rylko. All rights reserved.
//

#import "RSNewsManager.h"
#import "MNews.h"
#import "RSStoreManager.h"


@interface RSNewsManager ()

@property (nonatomic, strong) RKManagedObjectStore * aManagedObjectStore;

@end


@implementation RSNewsManager

- (instancetype)init
{
    self = [super init];
    if (self)
    {
        NSLog(@"Onliner Manager initiated");
        [self configureDataManager];
        [self setUpObjectStore];
        [self setNewsMapping];
    }
    return self;
}



#pragma mark - Store managing

- (void) setUpObjectStore
{
    RKManagedObjectStore * managedObjectStore = [[RSStoreManager sharedStoreManager] store];
    self.aManagedObjectStore = managedObjectStore;
    self.objectManager.managedObjectStore = managedObjectStore; //need
}



#pragma mark - Manage News

- (void) removeNews: (MNews *) news
{
    if (news == nil)
    {
        NSLog(@"%@", @"Nothing to remove");
        return;
    }
    NSLog(@"Removing News");

    NSManagedObjectContext * context = [[RSStoreManager sharedStoreManager] managedObjectContext];
    [context smartSaveBlock:^(NSManagedObjectContext *localContext) {
        [context deleteObject:news];
    }];
}



#pragma mark - Rest mapping

- (void) addMappingForEntityForName: (NSString *) entityName
andAttributeMappingsFromDictionnary: (NSDictionary *) attributeMappingsDictionary
        andIdentificationAttributes: (NSArray *) idAttributes
                     andPathPattern: (NSString *) pathPattern
                         andKeyPath: (NSString *) keyPathString
{
    NSLog(@"Mapping added");
    if (!self.aManagedObjectStore)
    {
        NSLog(@"NO MOS");
        return;
    }
    
    RKEntityMapping * entityMapping = [RKEntityMapping mappingForEntityForName:entityName inManagedObjectStore:self.aManagedObjectStore];
    
    [entityMapping addAttributeMappingsFromDictionary:attributeMappingsDictionary];
    entityMapping.identificationAttributes = idAttributes;
    
    RKResponseDescriptor * newsResponseDescriptor =
    [RKResponseDescriptor responseDescriptorWithMapping:entityMapping
                                                 method:RKRequestMethodAny
                                            pathPattern:pathPattern
                                                keyPath:keyPathString
                                            statusCodes:RKStatusCodeIndexSetForClass(RKStatusCodeClassSuccessful)];
    
    [self.objectManager addResponseDescriptor:newsResponseDescriptor];
}



#pragma mark - DataLoading

- (void) loadObjectsSuccess:(void (^)(void))success failure:(void (^)(void))failure
{
    [self.objectManager getObjectsAtPath:self.path
                              parameters:nil
                                 success:^(RKObjectRequestOperation *operation, RKMappingResult *mappingResult) {
                                     success();
                                 }
                                 failure:^(RKObjectRequestOperation *operation, NSError *error) {
                                     NSLog(@"ERROR GET OBJECTS -%@", [error localizedDescription]);
                                     failure();
                                 }];
}



#pragma mark - Fetched Result controller

- (NSFetchedResultsController*) configureNewsFetchResultController
{
    NSLog(@"Fetch res controller configured");
    
    NSFetchRequest * fetchRequest = [[NSFetchRequest alloc] initWithEntityName:NSStringFromClass([MNews class])];
    NSEntityDescription * entityDescription = [NSEntityDescription entityForName:NSStringFromClass([MNews class]) inManagedObjectContext:[[RSStoreManager sharedStoreManager] managedObjectContext]];
    NSSortDescriptor * firstDescriptor = [NSSortDescriptor sortDescriptorWithKey:@"mobNewsDate" ascending:NO];
    //setPredicate replaced by switchFetchPredicate

    [fetchRequest setEntity:entityDescription];
    [fetchRequest setSortDescriptors:@[firstDescriptor]];
    [fetchRequest setFetchLimit:300];
    [fetchRequest setFetchBatchSize:20];
    
    NSFetchedResultsController * fetchedResultsController = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest
                                                                                                managedObjectContext:[[RSStoreManager sharedStoreManager] managedObjectContext]
                                                                                                  sectionNameKeyPath:nil
                                                                                                           cacheName:nil];
    [self switchFetchPredicate:fetchedResultsController]; // setPredicate
    return fetchedResultsController;
}


- (void) switchFetchPredicate: (NSFetchedResultsController *) fetchedResultsController
{
    NSPredicate * predicate = [NSPredicate predicateWithFormat:@"mobDetailURL contains[c] %@", [self.objectManager.baseURL host]];
    NSLog(@"%@", [self.objectManager.baseURL host]);
    if (fetchedResultsController.fetchRequest.predicate != predicate)
    {
        [fetchedResultsController.fetchRequest setPredicate:predicate];        
    }
}



#pragma mark - Virtual methods

- (void) configureDataManager
{
    NSLog(@"ConfigureDataManager");
}


- (void) setNewsMapping
{
    NSLog(@"SetNewsMapping");
}


- (void) detailNewsTextFromURLString:(NSString *)urlString success:(void (^)(NSString *))success failure:(void (^)(NSError *))failure
{
    NSLog(@"virtual get detail");
}

/// tested
- (void) addSomeItems
{
    for (int i = 0; i < 1; i++)
    {
        NSManagedObjectContext * context = [[RSStoreManager sharedStoreManager] saveManagedObjectContext];
        [context smartSaveBlock:^(NSManagedObjectContext *localContext) {
            MNews * news = [NSEntityDescription insertNewObjectForEntityForName:NSStringFromClass([MNews class]) inManagedObjectContext:localContext];
            news.mobName = [NSString stringWithFormat:@"TETETETEST %u",i];
            news.mobDescription = @"Olalalalalala lalala lala" ;
            news.mobNewsDate = [NSDate dateWithTimeIntervalSinceNow:0];
            news.mobDetailURL = @"news.tut.by";
            NSLog(@"CURR THREAD - %@", [NSThread isMainThread] ? @"YES" : @"NO");
        }];

    }
}






@end
