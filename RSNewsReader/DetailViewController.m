//
//  RSDetailViewControlelr.m
//  RSNewsReader
//
//  Created by Сергей on 23.06.16.
//  Copyright © 2016 Serge Rylko. All rights reserved.
//

#import "DetailViewController.h"
#import <SDWebImage/UIImageView+WebCache.h>

@interface DetailViewController ()

@property (nonatomic, weak) IBOutlet UIScrollView * scrollView;
@property (nonatomic, weak) IBOutlet UIView * mainView;
@property (nonatomic, weak) IBOutlet UILabel * detailTextLabel;
@property (nonatomic, weak) IBOutlet UILabel * dateLabel;
@property (weak, nonatomic) IBOutlet UILabel * titleLabel;
@property (weak, nonatomic) IBOutlet UIImageView * detailImageView;

@end


@implementation DetailViewController


- (void)viewDidLoad
{
    [super viewDidLoad];    
    
    self.titleLabel.text = self.detailNews.mobName;    
    [self.detailImageView sd_setImageWithURL:[NSURL URLWithString:self.detailNews.mobIconImageURL]];
    self.detailTextLabel.text = self.detailNews.mobDetailNews;
    self.dateLabel.text = [NSString dateStringFromDate:[self.detailNews mobNewsDate]];

}

@end
