//
//  MasterViewController.m
//  RSNewsReader
//
//  Created by Сергей on 23.06.16.
//  Copyright © 2016 Serge Rylko. All rights reserved.
//

#import "MasterViewController.h"
#import "MasterViewController+testController.h"
#import "RSNewsManager.h"
#import "RSTutManager.h"
#import "RSOnlinerManager.h"
#import "RSCell.h"
#import "DetailViewController.h"
#import <SDWebImage/UIImageView+WebCache.h>


typedef enum {
    MasterViewControllerAlertNoNews,
    MasterViewControllerAlertNoDetailsNews
                } MasterViewControllerAlert;


@interface MasterViewController () <UITableViewDelegate, UITableViewDataSource, NSFetchedResultsControllerDelegate>

@property (nonatomic, strong) NSFetchedResultsController * fetchedResultController;
@property (nonatomic, strong) RSNewsManager * newsManager;

@property (nonatomic, weak) UIRefreshControl * refreshControl;
@property (nonatomic, weak) IBOutlet UITableView * tableView;

@property (nonatomic, weak) IBOutlet UISegmentedControl * switchNewsSourceSegmentedControl;


@end


@implementation MasterViewController


-(void)viewDidLoad
{
    [super viewDidLoad];
    [self addTestButton]; //// TESTED BUTTON

    [self.navigationItem setTitle:@"Новости"];
    
    self.tableView.rowHeight = UITableViewAutomaticDimension;
    self.tableView.estimatedRowHeight = 150.0f;
    
    UIRefreshControl *refreshControl = [[UIRefreshControl alloc] init];
    [refreshControl addTarget:self action:@selector(loadData) forControlEvents:UIControlEventValueChanged];
    self.refreshControl = refreshControl;
    [self.tableView addSubview:refreshControl];
    
    [self.switchNewsSourceSegmentedControl addTarget:self action:@selector(switchNews) forControlEvents:UIControlEventValueChanged];
    
    [self switchNews];
}


- (void) switchNews
{
    if (self.switchNewsSourceSegmentedControl.selectedSegmentIndex == 0)
    {
        self.newsManager = [[RSTutManager alloc] init];
    }
    else if (self.switchNewsSourceSegmentedControl.selectedSegmentIndex == 1)
    {
        self.newsManager = [[RSOnlinerManager alloc] init];
    }

    [self.newsManager switchFetchPredicate:self.fetchedResultController];
    
    NSError * error = nil;
    if (![self.fetchedResultController performFetch:&error])
    {
        NSLog(@"Fetched Controller Error ");
    }
    
    
    [self loadData];
    [self.tableView reloadData];//?? how to remove     
}



#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    id fetchedSection = [[self.fetchedResultController sections] objectAtIndex:section];
    return [fetchedSection numberOfObjects];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    RSCell  * cell = [self.tableView dequeueReusableCellWithIdentifier:RSCellMainIdentifier forIndexPath:indexPath];
    MNews * news = [self.fetchedResultController objectAtIndexPath:indexPath];
    
    [self configureCell:(RSCell*) cell withNews:news];
    
    return cell;
}



#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self animateActivityIndicator:YES];
    
    MNews * news = [self.fetchedResultController objectAtIndexPath:indexPath];
    if (!news.mobDetailURL)
    {
        [self animateActivityIndicator:NO];
        [self showAlert:MasterViewControllerAlertNoDetailsNews];
        return;
    }
    else if (!news.mobDetailNews)
    {
        [self.newsManager detailNewsTextFromURLString:news.mobDetailURL success:^(NSString *text)
         {
             dispatch_sync(dispatch_get_main_queue(), ^{
                 
                 [self animateActivityIndicator:NO];
                 news.mobDetailNews = text;
                 
                 [self performSegueWithIdentifier:NSStringFromClass([DetailViewController class]) sender:self];
             });
         } failure:^(NSError *error) {
             
             NSLog(@"Failure, error - %@", [error localizedDescription]);
             [self showAlert:MasterViewControllerAlertNoDetailsNews];
         }];
    }
    else
    {
        [self animateActivityIndicator:NO];
        [self performSegueWithIdentifier:NSStringFromClass([DetailViewController class]) sender:self];
    }
}


- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return UITableViewCellEditingStyleDelete;
}

- (nullable NSString *)tableView:(UITableView *)tableView titleForDeleteConfirmationButtonForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return @"Удалить новость";
}


- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete)
    {
        MNews * newsToDelete = [self.fetchedResultController objectAtIndexPath:indexPath];
        [self.newsManager removeNews:newsToDelete];
    }
    
}



#pragma mark - TableCell Generation

- (void) configureCell: (RSCell*) cell withNews: (MNews *) news
{
    cell.titleLabel.text = news.mobName;
    cell.dateLabel.text = [NSString dateStringFromDate:news.mobNewsDate];
    cell.descriptionLabel.text = [NSString stripTags:news.mobDescription];
    
    cell.iconImageView.image = nil;
    [cell.iconImageView sd_setImageWithURL:[NSURL URLWithString:news.mobIconImageURL]
                          placeholderImage:[UIImage imageNamed:@"tutby.png"]
                                 completed:nil];
}



#pragma mark - Segue

- (void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:NSStringFromClass([DetailViewController class])])
    {
        NSIndexPath * indexPath = [self.tableView indexPathForSelectedRow];
        DetailViewController * controller = segue.destinationViewController;
        controller.detailNews = [self.fetchedResultController objectAtIndexPath:indexPath];
    };
}



#pragma mark - News Data Source

- (void) loadData
{
    [self animateActivityIndicator:YES];
    
    [self.newsManager loadObjectsSuccess:^{
        
        [self animateActivityIndicator:NO];
        [self.refreshControl endRefreshing];
        
    } failure:^{
        
        [self.refreshControl endRefreshing];
        [self animateActivityIndicator:NO];
        [self showAlert:MasterViewControllerAlertNoNews];
    }];
}



#pragma mark - Lazy Getters

- (NSFetchedResultsController *)fetchedResultController
{
    if (_fetchedResultController == nil)
    {
        _fetchedResultController = [self.newsManager configureNewsFetchResultController];
        _fetchedResultController.delegate = self;
    }
    
    return _fetchedResultController;
}



#pragma mark - NSFetchedResultsControllerDelegate

- (void)controller:(NSFetchedResultsController *)controller didChangeObject:(id)anObject atIndexPath:(nullable NSIndexPath *)indexPath forChangeType:(NSFetchedResultsChangeType)type newIndexPath:(nullable NSIndexPath *)newIndexPath
{
    switch (type)
    {
        case NSFetchedResultsChangeInsert:
            [self.tableView insertRowsAtIndexPaths:[NSArray arrayWithObject:newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeDelete:
            [self.tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
        
        case NSFetchedResultsChangeMove:
            [self.tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
            [self.tableView insertRowsAtIndexPaths:[NSArray arrayWithObject:newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;

        case NSFetchedResultsChangeUpdate:
        {            
            [self.tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade]; // in case of changing description
            break;
        }
    }
}


- (void)controllerWillChangeContent:(NSFetchedResultsController *)controller
{
    [self.tableView beginUpdates]; // required while fetchedResultController initiated in ViewDidLoad
}


- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller
{
    [self.tableView endUpdates];
}



#pragma mark - AlertViewController

- (void) showAlert: (MasterViewControllerAlert) alert
{
    NSArray * alertMessageArray = @[@"Невозможно отобразить список новостей",   //MasterViewControllerAlertNoNews
                                    @"Отсутствует детальная новость"];          //MasterViewControllerAlertNoDetailsNews
    
    NSString * alertMessage = [alertMessageArray objectAtIndex:alert];
    
    UIAlertController * alertController = [UIAlertController alertControllerWithTitle:@"Ошибка"
                                                                              message:alertMessage
                                                                       preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];

    [alertController addAction:okAction];
    [self presentViewController:alertController animated:YES completion:nil];
}

@end
