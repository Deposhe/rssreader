//
//  NSManagedObjectContext+RSSaving.m
//  RSNewsReader
//
//  Created by Mobexs on 7/22/16.
//  Copyright © 2016 Serge Rylko. All rights reserved.
//

#import "NSManagedObjectContext+RSSaving.h"
#import "NSManagedObjectContext+RKAdditions.h"

@implementation NSManagedObjectContext (RSSaving)

- (void) smartSaveBlock: (void (^) (NSManagedObjectContext * localContext)) saveBlock
{
    NSManagedObjectContext * context = self;
    BOOL saveToPersistentStore = context.concurrencyType ? NSPrivateQueueConcurrencyType : NSMainQueueConcurrencyType;
    
    [context performBlockAndWait:^{
        saveBlock (context);
        
        NSError * error = nil;
        if (saveToPersistentStore)
        {
            if ([context hasChanges] && ![context saveToPersistentStore: nil])
                {
                    NSLog(@"Unresolved error saveToPersistentStore %@, %@", error, [error userInfo]);
                }
            
        }
        else
        {
            if ([context hasChanges] && ![context save:&error])
            {
                NSLog(@"Unresolved error context save %@, %@", error, [error userInfo]);
            }
        }
    }];
}

@end
