//
//  RSTutManager.h
//  RSNewsReader
//
//  Created by Mobexs on 7/15/16.
//  Copyright © 2016 Serge Rylko. All rights reserved.
//

#import "RSNewsManager.h"

@interface RSTutManager : RSNewsManager <RSNewsManagerProtocol>

@end
