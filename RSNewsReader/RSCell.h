//
//  RSCell.h
//  RSNewsReader
//
//  Created by Сергей on 21.06.16.
//  Copyright © 2016 Serge Rylko. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MNews.h"

extern NSString * RSCellMainIdentifier;


@interface RSCell : UITableViewCell

@property (nonatomic, weak) IBOutlet UIImageView * iconImageView;
@property (nonatomic, weak) IBOutlet UILabel * titleLabel;
@property (nonatomic, weak) IBOutlet UILabel * dateLabel;
@property (nonatomic, weak) IBOutlet UILabel * descriptionLabel;

@end
