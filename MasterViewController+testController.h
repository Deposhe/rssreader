//
//  MasterViewController+testController.h
//  RSNewsReader
//
//  Created by Mobexs on 7/13/16.
//  Copyright © 2016 Serge Rylko. All rights reserved.
//

#import "MasterViewController.h"

@interface MasterViewController (testController)
/*
- (void) addTestFeatures;
- (void) changeNews: (NSInteger) indexOfNews;
*/


- (void) changeNewsName;

- (void) addTestButton;

@end
